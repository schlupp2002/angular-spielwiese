import { TohHttpClientPage } from './app.po';

describe('toh-http-client App', function() {
  let page: TohHttpClientPage;

  beforeEach(() => {
    page = new TohHttpClientPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
