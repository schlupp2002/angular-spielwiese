import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {Routes, RouterModule} from "@angular/router";

import {AppHomeComponent} from "./app-home/app-home.component";
import {PageNotFoundComponent} from "./page-not-found/page-not-found.component";
import {ProfileComponent} from "./profile/profile.component";

const ROOT_ROUTES: Routes = [
  {
    path: '',
    component: AppHomeComponent,
  },
  {
    path: '**',
    component: PageNotFoundComponent,
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(ROOT_ROUTES),
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})

export class AppRoutingModule {
}
