import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';

import {AppRoutingModule} from "./app-routing.module";

import {AppComponent} from './app.component';

import {PageNotFoundModule} from "./page-not-found/page-not-found.module";
import { AppHomeComponent } from './app-home/app-home.component';
import { ProfileComponent } from './profile/profile.component';
import {ProfileModule} from "./profile/profile.module";

@NgModule({
  declarations: [
    AppComponent,
    AppHomeComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,

    PageNotFoundModule,
    ProfileModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
