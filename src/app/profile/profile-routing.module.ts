import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from "@angular/router";
import {ProfileHomeComponent} from "./profile-home/profile-home.component";
import {ProfileListComponent} from "./profile-list/profile-list.component";
import {ProfileEditComponent} from "./profile-edit/profile-edit.component";
import {ProfileComponent} from "./profile.component";
import {ProfileShowComponent} from "./profile-show/profile-show.component";

const PROFILE_ROUTES : Routes = [

  {
    path: 'profile',
    component: ProfileComponent,
    children: [
      {
        path: 'list',
        component: ProfileListComponent,
        children: [
          {
            path: ':id',
            component: ProfileEditComponent,
          },
        ]
      },
      {
        path: 'edit',
        component: ProfileEditComponent,
      },
      {
        path: 'show',
        component: ProfileShowComponent,
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(PROFILE_ROUTES)
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class ProfileRoutingModule { }
