import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileHomeComponent } from './profile-home/profile-home.component';
import {ProfileRoutingModule} from "./profile-routing.module";
import { ProfileShowComponent } from './profile-show/profile-show.component';
import { ProfileEditComponent } from './profile-edit/profile-edit.component';
import { ProfileListComponent } from './profile-list/profile-list.component';
import {ProfileComponent} from "./profile.component";

@NgModule({
  imports: [
    CommonModule,
    ProfileRoutingModule
  ],
  declarations: [
    ProfileComponent,
    ProfileHomeComponent,
    ProfileShowComponent,
    ProfileEditComponent,
    ProfileListComponent]
})
export class ProfileModule { }
